'use strict';
const port                = process.env.PORT || 3000;
const bodyParser          = require('body-parser');
const methodOverride      = require('method-override');
const { express, router } = require('./src/express/config');
let app                   = express();

// ONLY FOR CORSE ON SAME PC USING REACT FOR EXAMPLE
app.use( (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// COMMON SETTINGS
app.use(router);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());


// INIT SERVER
app.listen(port, () => {
  console.log(`Node server working on http://localhost:${port}`);
});