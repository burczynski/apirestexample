'use strict';
const path = require('path');
const data = require('../data/movies');

// RETURN COMPLETE JSON
async function index() {
	try {
		return data;
	} catch(e) {
		console.log(`[error] index function ${e} at path: ${path}`);
	}
}

// RETURN SINGLE ITEM
async function id(id) {
	try {
		let movie = await search(id);
		return movie;
	} catch(e) {
		console.log(`[error] id function ${e} at path: ${path}`);
	}
}

// ADD NEW ITEM
async function post(params) {
	try {
		// check all params to do it right before add in movies.json
		return params;
	} catch(e) {
		console.log(`[error] post function ${e} at path: ${path}`);
	}
}

// UPDATE SINGLE ITEM
async function put(params) {
	try {
		// check all params to update, this example just set movie active to true
		let result      = false;
		let episode     = params.episode_number;
		let movie       = await search(episode);
		if (movie != undefined) {
			movie.active = true;
			result = movie;
		}
		return result;
	} catch(e) {
		console.log(`[error] put function ${e} at path: ${path}`);
	}
}

// DELETE SINGLE ITEM
async function deleteId(id) {
	try {
		// change movie active to false in movies.json
		let result = false;
		let movie  = await search(id);

		if (movie != undefined) {
			movie.active = false;
			result = true;
		}
		return result;
	} catch(e) {
		console.log(`[error] deleteId function ${e} at path: ${path}`);
	}
}

// SEARCH BY DATA PARAMETER
async function search(id) {
	try {
		const result = data.movies.find( movie => movie.episode_number === id );
		return result;
	} catch(e) {
		console.log(`[error] search function ${e} at path: ${path}`);
	}
}

module.exports = {
	index,
	id,
	post,
	put,
	deleteId
}