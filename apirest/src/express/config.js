'use strict';
const express      = require('express');
const router       = express.Router();
const routeModules = require('./routeModules');

// GET ALL RESULTS
router.get('/', async (req, res) => {
	let data = await routeModules.index();
	res.send(data);
});

// GET SINGLE RESULT
router.get('/episode/:id', async (req, res) => {
	let data = await routeModules.id(req.params.id);

	if (data === undefined) {
		return res.status(404).send({
		   Error: 'Data not found'
		});
	} else {
		res.send(data);
	}
});

// ADD NEW ITEM
router.post('/episode', async (req, res) => {
	let data = await routeModules.post(req.query);

	if (data === false) {
		return res.status(404).send({
			Error: 'Data id not found'
		});
	} else {
		res.status(200).send({
			success: true,
			message: "New movie added",
			data   : data
		});
	}
});

// UPDATE SINGLE ITEM
router.put('/episode/:id', async (req, res) => {
	let data = await routeModules.put(req.query);

	if (data === false) {
		return res.status(404).send({
			Error: 'Data id not found'
		});
	} else {
		res.status(200).send({
			success: true,
			message: "Movie update",
			data   : data
		});
	}
});

// DELETE SINGLE ITEM
router.delete('/episode/:id', async(req, res) => {
	let data = await routeModules.deleteId(req.params.id);

	if (data === false) {
		return res.status(404).send({
			Error: 'Data id not found'
		});
	} else {
		res.status(200).send({
			success: true,
			message: "movie status modified"
		});
	}
});

module.exports = {
	express,
	router
}